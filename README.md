
# Build basic external tools for TDAQ

Do not set up any TDAQ or ATLAS release ! Just use
the system tools for this.

```bash
git clone ...
mkdir build
cd build
cmake ..
make
```
Then deploy the tar ball on /cvmfs.

There is no need to rebuild existing versions. You can specify
the list of version on the command line, e.g. to just build a
new git version:

```bash
cmake -D Git_VERSION_LIST="2.27.0" -D CMake_VERSION_LIST="" ..
make
```

You can modify the default CVMFS installation path by re-defining
`CVMFS_INSTALL_PREFIX`.

```bash
cmake -D CMVFS_INSTALL_PREFIX=/sw/atlas/tools [other options] ..
```

Alternatively, run `make help` after the `cmake` step, and if the
version you want is available, just build that one:

```bash
make Git-2.25.2
make tarfile
```
